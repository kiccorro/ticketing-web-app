﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace TicketingWebApp.Model
{
    public class TicketDbContext : IdentityDbContext
    {
        public TicketDbContext(DbContextOptions<TicketDbContext> options) : base(options)
        {
        }

        public virtual DbSet<LogEvent> LogEvents { get; set; }
        public virtual DbSet<Ticket> Tickets { get; set; }
        public virtual DbSet<TicketComment> TicketComments { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<LogEvent>(entity =>
            {
                entity.ToTable("LogEvent");

                entity.Property(e => e.CreatedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description)
                        .IsRequired()
                        .IsUnicode(false);

                entity.Property(e => e.Type)
                        .IsRequired()
                        .HasMaxLength(100)
                        .IsUnicode(false);
            });

            modelBuilder.Entity<Ticket>(entity =>
            {
                entity.ToTable("Ticket");

                entity.Property(e => e.CreatedByUserId)
                    .IsRequired()
                    .HasMaxLength(450);

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                //entity.HasOne(d => d.CreatedByUserId)
                //    .WithMany(p => p.Ticket)
                //    .hasforeignkey(d => d.createdbyuserid)
                //    .ondelete(deletebehavior.clientsetnull)
                //    .hasconstraintname("fk__ticket__createdb__6fe99f9f");
            });

            modelBuilder.Entity<TicketComment>(entity =>
            {
                entity.ToTable("TicketComment");

                entity.Property(e => e.CreatedByUserId)
                    .IsRequired()
                    .HasMaxLength(450);

                entity.Property(e => e.CreatedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Text)
                    .IsRequired()
                    .IsUnicode(false);

                //entity.HasOne(d => d.CreatedByUser)
                //    .WithMany(p => p.TicketComments)
                //    .HasForeignKey(d => d.CreatedByUserId)
                //    .OnDelete(DeleteBehavior.ClientSetNull)
                //    .HasConstraintName("FK__TicketCom__Creat__73BA3083");

                entity.HasOne(d => d.Ticket)
                    .WithMany(p => p.TicketComments)
                    .HasForeignKey(d => d.TicketId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TicketCom__Ticke__72C60C4A");
            });

            base.OnModelCreating(modelBuilder);

        }
    }
}