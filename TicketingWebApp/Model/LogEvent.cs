﻿using System;

namespace TicketingWebApp.Model
{
    public partial class LogEvent
    {
        
        public DateTime CreatedDateTime { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public int Id { get; set; }
    }
}
