﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

#nullable disable

namespace TicketingWebApp.Model
{
    public partial class TicketComment
    {
        public int Id { get; set; }
        public int TicketId { get; set; }
        public string CreatedByUserId { get; set; }
        [NotMapped]
        public string CreatedByUserName { get; set; }

        public DateTime CreatedDateTime { get; set; }

        [Required]
        public string Text { get; set; }

        //public virtual AspNetUser CreatedByUser { get; set; }
        public virtual Ticket Ticket { get; set; }
    }
}