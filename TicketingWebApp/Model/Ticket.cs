﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

#nullable disable

namespace TicketingWebApp.Model
{
    public partial class Ticket
    {
        public Ticket()
        {
            TicketComments = new HashSet<TicketComment>();
        }

        public int Id { get; set; }
        public string CreatedByUserId { get; set; }

        [NotMapped]
        [DisplayName("User")]
        public string CreatedByUserName { get; set; }

        [Required]
        [StringLength(250)]
        [RegularExpression(@"^[A-Za-z0-9!,. ]*$")]
        public string Title { get; set; }

        [Required]
        //[RegularExpression(@"^[A-Z]+[a-zA-Z\s]*$")]
        public string Description { get; set; }
        
        //public virtual AspNetUser CreatedByUser { get; set; }
        public virtual ICollection<TicketComment> TicketComments { get; set; }
    }
}