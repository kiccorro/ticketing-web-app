
/* Delete all */
DELETE FROM [TicketDb].[dbo].[TicketComment]
WHERE TicketId IN 
	(SELECT [TicketDb].[dbo].[Ticket].id FROM [TicketDb].[dbo].[Ticket]
	INNER JOIN [TicketDb].[dbo].[AspNetUsers]
		ON [TicketDb].[dbo].[Ticket].CreatedByUserId = [TicketDb].[dbo].[AspNetUsers].Id
	)

DELETE FROM [TicketDb].[dbo].[TicketComment]
WHERE CreatedByUserId IN 
	(SELECT [TicketDb].[dbo].[AspNetUsers].Id FROM [TicketDb].[dbo].[AspNetUsers]
	)

DELETE FROM [TicketDb].[dbo].[Ticket]
WHERE Id IN 
	(SELECT [TicketDb].[dbo].[Ticket].id FROM [TicketDb].[dbo].[Ticket]
	INNER JOIN [TicketDb].[dbo].[AspNetUsers]
		ON [TicketDb].[dbo].[Ticket].CreatedByUserId = [TicketDb].[dbo].[AspNetUsers].Id
	)

DELETE FROM [TicketDb].[dbo].[AspNetUsers]
WHERE Id IN 
	(SELECT [TicketDb].[dbo].[AspNetUsers].Id FROM [TicketDb].[dbo].[AspNetUsers]
	)

TRUNCATE TABLE [TicketDb].[dbo].[LogEvent]

/* Selected User related information
	
DELETE FROM [TicketDb].[dbo].[TicketComment]
WHERE TicketId IN 
	(SELECT [TicketDb].[dbo].[Ticket].id FROM [TicketDb].[dbo].[Ticket]
	INNER JOIN [TicketDb].[dbo].[AspNetUsers]
		ON [TicketDb].[dbo].[Ticket].CreatedByUserId = [TicketDb].[dbo].[AspNetUsers].Id
	WHERE [TicketDb].[dbo].[AspNetUsers].UserName IN ('Account3','Account2','Account1','admin','KyleAccount')
	)

DELETE FROM [TicketDb].[dbo].[TicketComment]
WHERE CreatedByUserId IN 
	(SELECT [TicketDb].[dbo].[AspNetUsers].Id FROM [TicketDb].[dbo].[AspNetUsers]
	WHERE [TicketDb].[dbo].[AspNetUsers].UserName IN ('Account3','Account2','Account1','admin','KyleAccount')
	)

DELETE FROM [TicketDb].[dbo].[Ticket]
WHERE Id IN 
	(SELECT [TicketDb].[dbo].[Ticket].id FROM [TicketDb].[dbo].[Ticket]
	INNER JOIN [TicketDb].[dbo].[AspNetUsers]
		ON [TicketDb].[dbo].[Ticket].CreatedByUserId = [TicketDb].[dbo].[AspNetUsers].Id
	WHERE [TicketDb].[dbo].[AspNetUsers].UserName IN ('Account3','Account2','Account1','admin','KyleAccount')
	)


DELETE FROM [TicketDb].[dbo].[AspNetUsers]
WHERE Id IN 
	(SELECT [TicketDb].[dbo].[AspNetUsers].Id FROM [TicketDb].[dbo].[AspNetUsers]
	WHERE [TicketDb].[dbo].[AspNetUsers].UserName IN ('Account3','Account2','Account1','admin','KyleAccount')
	)

*/