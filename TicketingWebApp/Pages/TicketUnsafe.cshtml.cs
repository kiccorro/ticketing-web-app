﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using TicketingWebApp.Model;

namespace TicketingWebApp.Pages
{
    [Authorize]
    public class TicketUnsafeModel : PageModel
    {
        private readonly TicketingWebApp.Model.TicketDbContext _context;
        private UserManager<IdentityUser> userManager;


        public TicketUnsafeModel(TicketingWebApp.Model.TicketDbContext context, UserManager<IdentityUser> userManager)
        {
            _context = context;
            this.userManager = userManager;
        }

        public Ticket Ticket { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            // Check if authorized
            var tempTicket = await _context.Tickets.FirstOrDefaultAsync(m => m.Id == id);
            if (tempTicket == null)
            {
                return NotFound();
            }
            if (userManager.GetUserName(User) == "admin" || userManager.GetUserId(User) == tempTicket.CreatedByUserId)
            {
                Ticket = await _context.Tickets.FirstOrDefaultAsync(m => m.Id == id);
            }

            if (Ticket == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
