﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TicketingWebApp.Model;

namespace TicketingWebApp.Pages
{
    [Authorize]
    public class EditTicketModel : PageModel
    {
        private readonly TicketingWebApp.Model.TicketDbContext _context;
        private UserManager<IdentityUser> userManager;

        public EditTicketModel(TicketingWebApp.Model.TicketDbContext context, UserManager<IdentityUser> userManager)
        {
            _context = context;
            this.userManager = userManager;
        }

        [BindProperty]
        public Ticket Ticket { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            // Check if authorized
            var tempTicket = await _context.Tickets.FirstOrDefaultAsync(m => m.Id == id);
            if (tempTicket == null)
            {
                return NotFound();
            }
            if (userManager.GetUserName(User) == "admin" || userManager.GetUserId(User) == tempTicket.CreatedByUserId)
            {
                Ticket = await _context.Tickets.FirstOrDefaultAsync(m => m.Id == id);
            }

            if (Ticket == null)
            {
                return NotFound();
            }
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            // Check if authorized
            var tempTicket = await _context.Tickets.FirstOrDefaultAsync(m => m.Id == Ticket.Id);
            if (tempTicket == null)
            {
                return NotFound();
            }
            if (userManager.GetUserName(User) == "admin" || userManager.GetUserId(User) == tempTicket.CreatedByUserId)
            {
                //Log Event
                _context.LogEvents.Add(new LogEvent() { Type = "Ticket Updated", Description = $"Id:{Ticket.Id} Title:{Ticket.Title};Description:{Ticket.Description};" });
                //Update
                _context.ChangeTracker.Clear();
                _context.Attach(Ticket).State = EntityState.Modified;
            }
            else
            {
                return NotFound();
            }

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)

            {
                if (!TicketExists(Ticket.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            //return RedirectToPage("./ViewTickets");
            return RedirectToPage("./Ticket/", new { id = Ticket.Id });
        }

        private bool TicketExists(int id)
        {
            return _context.Tickets.Any(e => e.Id == id);
        }
    }
}
