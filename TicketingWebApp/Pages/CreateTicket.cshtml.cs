using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Threading.Tasks;
using TicketingWebApp.Model;

namespace TicketingWebApp.Pages
{
    [Authorize]
    public class CreateTicketModel : PageModel
    {
        [BindProperty]
        public Ticket Model { get; set; }

        TicketDbContext ticketDbContext;
        UserManager<IdentityUser> userManager;

        public CreateTicketModel(TicketDbContext ticketDbContext, UserManager<IdentityUser> userManager)
        {
            this.ticketDbContext = ticketDbContext;
            this.userManager = userManager;
        }

        public void OnGet()
        {
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (ModelState.IsValid)
            {
                Model.CreatedByUserId = userManager.GetUserId(User);
                ticketDbContext.Tickets.Add(this.Model);
                // Log event
                await ticketDbContext.SaveChangesAsync();
                ticketDbContext.LogEvents.Add(new LogEvent() { Type = "Ticket Created", Description = $"Id:{Model.Id};Title:{Model.Title};Description:{Model.Description};" });
                await ticketDbContext.SaveChangesAsync();
                return RedirectToPage("ViewTickets");
            }
            return Page();
        }
    }
}