using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Threading.Tasks;
using TicketingWebApp.Model;

namespace TicketingWebApp.Pages
{
    public class LogoutModel : PageModel
    {
        private readonly SignInManager<IdentityUser> signInManager;
        private UserManager<IdentityUser> userManager;
        private TicketDbContext ticketDbContext;

        public LogoutModel(SignInManager<IdentityUser> signInManager, TicketDbContext ticketDbContext, UserManager<IdentityUser> userManager)
        {
            this.signInManager = signInManager;
            this.userManager = userManager;
            this.ticketDbContext = ticketDbContext;
        }

        public void OnGet()
        {
        }

        public async Task<IActionResult> OnPostLogoutAsync()
        {
            // Log event
            ticketDbContext.LogEvents.Add(new LogEvent() { Type = "Logout", Description = $"Username:{userManager.GetUserName(User)};" });
            await ticketDbContext.SaveChangesAsync();
            // Sign out
            await signInManager.SignOutAsync();
            return RedirectToPage("Login");
        }

        public IActionResult OnPostDontLogoutAsync()
        {
            return RedirectToPage("Index");
        }
    }
}
