using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicketingWebApp.Model;

namespace TicketingWebApp.Pages
{
    [Authorize]
    public class ViewTicketsModel : PageModel
    {
        public IEnumerable<Ticket> tickets;
        private TicketDbContext _ticketDbContext;
        private UserManager<IdentityUser> userManager;

        public ViewTicketsModel(TicketDbContext ticketDbContext, UserManager<IdentityUser> userManager)
        {
            _ticketDbContext = ticketDbContext;
            this.userManager = userManager;
        }

        public async Task<IActionResult> OnGetAsync()
        {
            if (userManager.GetUserName(User) == "admin")
            {
                tickets = _ticketDbContext.Tickets;
            }
            else
            {
                tickets = _ticketDbContext.Tickets.Where(t => t.CreatedByUserId == userManager.GetUserId(User));
            }
            
            // Fill in user names
            foreach (var ticket in tickets)
            {
                var user = await userManager.FindByIdAsync(ticket.CreatedByUserId);
                ticket.CreatedByUserName = user.UserName;

                ticket.TicketComments = _ticketDbContext.TicketComments.Where(c => c.TicketId == ticket.Id).ToList();
            }

            return Page();
        }
    }
}
