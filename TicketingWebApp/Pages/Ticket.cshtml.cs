﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using TicketingWebApp.Model;

namespace TicketingWebApp.Pages
{
    [Authorize]
    public class TicketModel : PageModel
    {
        private readonly TicketingWebApp.Model.TicketDbContext _context;
        private UserManager<IdentityUser> userManager;

        public TicketModel(TicketingWebApp.Model.TicketDbContext context, UserManager<IdentityUser> userManager)
        {
            _context = context;
            this.userManager = userManager;
        }

        public Ticket Ticket { get; set; }

        public IEnumerable<TicketComment> TicketComments { get; set; }

        [BindProperty]
        public TicketComment Model { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            // Check if authorized
            var tempTicket = await _context.Tickets.FirstOrDefaultAsync(m => m.Id == id);
            if (tempTicket == null)
            {
                return NotFound();
            }
            if (userManager.GetUserName(User) == "admin" || userManager.GetUserId(User) == tempTicket.CreatedByUserId)
            {
                Ticket = await _context.Tickets.FirstOrDefaultAsync(m => m.Id == id);
                TicketComments = _context.TicketComments.Where(m => m.TicketId == id);
            }

            if (Ticket == null)
            {
                return NotFound();
            }

            // Fill in user names
            var user = await userManager.FindByIdAsync(Ticket.CreatedByUserId);
            Ticket.CreatedByUserName = user.UserName;
            foreach (var comment in TicketComments)
            {
                user = await userManager.FindByIdAsync(comment.CreatedByUserId);
                comment.CreatedByUserName = user.UserName;
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (ModelState.IsValid)
            {
                Ticket = await _context.Tickets.FirstOrDefaultAsync(m => m.Id == Model.TicketId);
                TicketComment newComment = new TicketComment();
                newComment.TicketId = Model.TicketId;
                newComment.Text = Model.Text;
                newComment.CreatedByUserId = userManager.GetUserId(User);
                Ticket.TicketComments.Add(newComment);
                _context.Tickets.Update(Ticket);

                // Log event
                _context.LogEvents.Add(new LogEvent() { Type = "Comment Created", Description = $"TicketId:{newComment.TicketId};UserId:{newComment.CreatedByUserId}Text:{newComment.Text}" });

                await _context.SaveChangesAsync();
            }

            // Get Data
            Ticket = await _context.Tickets.FirstOrDefaultAsync(m => m.Id == id);
            TicketComments = _context.TicketComments.Where(m => m.TicketId == id);
            // Fill in user names
            var user = await userManager.FindByIdAsync(Ticket.CreatedByUserId);
            Ticket.CreatedByUserName = user.UserName;
            foreach (var comment in TicketComments)
            {
                user = await userManager.FindByIdAsync(comment.CreatedByUserId);
                comment.CreatedByUserName = user.UserName;
            }
            //return Page();
            return RedirectToPage("Ticket", new { id = id });
        }
    }
}
