using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Threading.Tasks;
using TicketingWebApp.Model;
using TicketingWebApp.ViewModel;

namespace TicketingWebApp.Pages
{
    public class LoginModel : PageModel
    {
        [BindProperty]
        public Login Model { get; set; }


        private readonly SignInManager<IdentityUser> signInManager;
        private readonly TicketDbContext ticketDbContext;

        public LoginModel(SignInManager<IdentityUser> signInManager, TicketDbContext ticketDbContext)
        {
            this.signInManager = signInManager;
            this.ticketDbContext = ticketDbContext;
        }

        public void OnGet()
        {
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            if (ModelState.IsValid)
            {
                var identityResult = await signInManager.PasswordSignInAsync(Model.Username, Model.Password, Model.RememberMe, true);
                if (identityResult.Succeeded)
                {
                    // Log event
                    ticketDbContext.LogEvents.Add(new LogEvent() { Type = "Login Success", Description = $"Username:{Model.Username};" });
                    await ticketDbContext.SaveChangesAsync();

                    if (returnUrl == null || returnUrl == "/")
                    {
                        return RedirectToPage("Index");
                    }
                    else
                    {
                        return RedirectToPage(returnUrl);
                    }
                }
                else
                {
                    // Log event
                    ticketDbContext.LogEvents.Add(new LogEvent() { Type = "Login Failed", Description = $"Username:{Model.Username};" });
                    await ticketDbContext.SaveChangesAsync();
                }

                ModelState.AddModelError("", "Username or Password incorrect");
            }

            return Page();
        }
    }
}
