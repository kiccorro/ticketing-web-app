CREATE DATABASE TicketDb;

USE [TicketDb];

CREATE TABLE Ticket (
Id INT NOT NULL Identity(1,1) PRIMARY KEY,
CreatedByUserId nvarchar(450) NOT NULL, 
Title VARCHAR(255) NOT NULL,
Description VARCHAR(MAX),
);

CREATE TABLE TicketComment (
Id INT NOT NULL Identity(1,1) PRIMARY KEY,
TicketId INT NOT NULL FOREIGN KEY REFERENCES Ticket(Id),
CreatedByUserId nvarchar(450) NOT NULL,
CreatedDateTime DateTime NOT NULL DEFAULT(GETDATE()),
Text VARCHAR(MAX) NOT NULL,
);

CREATE TABLE LogEvent (
CreatedDateTime DateTime NOT NULL DEFAULT(GETDATE()),
Type VARCHAR(100) NOT NULL,
Description VARCHAR(MAX) NOT NULL,
Id int Identity(1,1) PRIMARY KEY,
);